function [xih] = Tikhonov(A,yh,alpha)
    % TIKHONOV Returns Tikhonov-regularised solution of the discrete inverse problem
    % A*xih = yh using the regularisation parameter alpha.
    % Using LSQ characterisation of the Tikhonov solution.

    n = min(size(A));
    B = [A ; alpha*eye(n)];
    b = [yh ; zeros(n,1)];
   
    %potentially replace by an implementation using Householder matrices
    % not sure how Matlab calculates this!
    xih = B\b;
    
    %{
    Alternatively:
    xih2 = 0;
    for i=1:n
        si = S(i,i);
        if (si ~= 0)
            xih2 = xih2 + g(si,alpha)/si*(U(:,i)'*yh)*V(:,i);
        end
    end
    %}
end


