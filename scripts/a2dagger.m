function [res] = a2dagger(A,y)
    % A2DAGGER calculates norm(A^{\dagger\dagger}y,2)
    % This value relates to the smoothness of the data y.
    n = min(size(A));
    
    [U,S,V] = svd(A);
    a2dagger = 0;
    for i=1:n
        si = S(i,i);
        if si ~= 0
            a2dagger = a2dagger + 1/si^2*(U(:,i)'*y)*V(:,i);
        end
    end
    res = a2dagger;
end