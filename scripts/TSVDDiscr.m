function [min_alpha] = TSVDDiscr(eps,A,y)
    % TSVDDISCR determines an optimal parameter alpha.
    % The parameter alpha is chosen as defined by the discrepancy
    % principle.
    
    n = min(size(A));
    
    d = @(alpha)(norm(A*TSVD(A,y,alpha)-y,2));
    func = @(alpha)(abs(d(alpha)-eps));
    
    sv = svds(A,n);
    
    alpha = sv(2);
    min_err = func(alpha);
    min_alpha = alpha;
    
    for i=3:n
        if func(alpha) < min_err
            min_err = func(alpha);
            min_alpha = alpha;
        end
        alpha = sv(i);
    end
    
    %{
    n = 20;
    x=linspace(0,1,n);
    y=x;
    for i=1:n
        y(i) = d(x(i));
    end
    plot(x,y);
    grid
    %}
    
end