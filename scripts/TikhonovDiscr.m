function [alpha] = TikhonovDiscr(eps,A,y)
    % TIKHONOVDISCR determines an optimal parameter alpha.
    % The parameter alpha is chosen as defined by the discrepancy
    % principle.

    d = @(alpha)(norm(A*Tikhonov(A,y,alpha)-y,2)^2);
    func = @(alpha)(d(alpha)-eps^2);
    
    %{
    n = 20;
    x=linspace(0,1,n);
    y=x;
    for i=1:n
        y(i) = d(x(i));
    end
    plot(x,y);
    grid
    %}
    
    alpha = fzero(func,0);
    d(alpha)
    eps^2
    
end