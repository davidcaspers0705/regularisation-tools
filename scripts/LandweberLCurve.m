function [] = LandweberLCurve(A,y,k)
    d = @(alpha)(norm(A*Landweber(A,y,alpha,y)-y,2)^2);
    psi = @(alpha)(norm(Landweber(A,y,alpha,y),2)^2);
    
    alpha = linspace(1,k,k);
    da = alpha;
    psia = alpha;
    
    n = min(size(A));

    sv = svds(A,n);
    beta = 1.9/norm(A)^2;
    xik = zeros(n,1);
    
    for i=1:k
        xik = xik - beta*A'*(A*xik-y);
        da(i) = norm(A*xik-y,2)^2;
        psia(i) = norm(xik,2)^2;
    end
    
    
    plot(da(100:end),psia(100:end),'x');
    legend('$(d(k),\psi(k))$','Interpreter','latex');

end