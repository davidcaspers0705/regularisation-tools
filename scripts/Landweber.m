function [xik] = Landweber(A,yh,k,exact_sol)
%LANDWEBER Performs Landweber iterations for the inverse problem A*xik=yh
%   Returns the kth Landweber iterand.

% Right now, the method decides on its own convergence parameter beta and
% This makes sense because it requires knowledge of the greatest singular
% value. Possibly make this an extra function argument? 
% Same question regarding the initial vector xi0.

%filter function
g = @(k,z,beta)((0 <= z <= beta^(-1/2)).*(1-(1-beta.*z.^2)^k)+(z > beta^(-1/2)));


n = min(size(A));

sv = svds(A,n);

beta = 0.9/norm(A)^2
norm(A)^2
norm(A'*A)
sv(1)^2

xik = zeros(n,1);
cbeta = norm(eye(n)-beta*(A'*A),2)
check = svds(eye(n)-beta*(A'*A),1)


for i=1:k
    err = norm(exact_sol-xik);
    err_bound = cbeta^i*norm(exact_sol,2);
    new = (A*xik-yh);
    xik = xik - beta*A'*(A*xik-yh);
end

%{
xik = 0;
[U,S,V] = svd(A);
for j=1:n
    if (sv(j) ~= 0)
        beta^(-1/2)
        xik = xik + g(k,sv(j),beta)/sv(j)*(U(:,j)'*yh)*V(:,j)
    end
end
%}

end

