function [] = TSVDLCurve(A,y,alpha_min,alpha_max,n)
    d = @(alpha)(norm(A*TSVD(A,y,alpha)-y,2)^2);
    psi = @(alpha)(norm(TSVD(A,y,alpha),2)^2);
    
    [U,S,V] = svd(A);
    alphas = zeros(n,1);
    all_regs = zeros(n,n);
    da = alphas;
    psia = alphas;
    
    for k=2:n
        alphas(k-1) = S(k,k);
        all_regs(:,k-1) = TSVD(A,y,alphas(k-1));
    end
    %add unregularised problem:
    all_regs(:,n) = A\y;
    alphas(n) = 0;
    
    for i=1:n
        da(i) = d(alphas(i));
        psia(i) = psi(alphas(i));
    end
    
    loglog(da,psia,'x','MarkerSize',12);
    legend('$(d(\alpha),\psi(\alpha))$','Interpreter','latex');
    set( gca, 'Box','off',...
    'TickDir','out',...
    'TickLength',[.02 .02],...
    'XMinorTick','off',...
    'YMinorTick','off')
set( gcf, 'PaperPositionMode','auto')
    table(alphas,da,psia)

end
