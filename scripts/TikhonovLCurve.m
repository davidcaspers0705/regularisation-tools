function [] = TikhonovLCurve(A,y,alpha_min,alpha_max,n)
    d = @(alpha)(norm(A*Tikhonov(A,y,alpha)-y,2)^2);
    psi = @(alpha)(norm(Tikhonov(A,y,alpha),2)^2);
    
    alpha = linspace(alpha_min,alpha_max,n);
    da = alpha;
    psia = alpha;
    
    for i=1:n
        da(i) = (d(alpha(i)));
        psia(i) = (psi(alpha(i)));
    end
    
    loglog(da,psia,'x','MarkerSize',12);
    legend('$(d(\alpha),\psi(\alpha))$','Interpreter','latex');
    table(alpha',da',psia')

end