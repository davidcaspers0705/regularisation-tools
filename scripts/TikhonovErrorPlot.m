function [errs] = TikhonovErrorPlot(A,y,alpha_min,alpha_max,n,exact)
    norm_func = @(alpha)(norm(Tikhonov(A,y,alpha)-exact,2));
   
    alpha = linspace(alpha_min,alpha_max,n);
    norms = alpha;
    
    for i=1:n
        norms(i) = norm_func(alpha(i));
    end
    loglog(alpha,norms,'LineWidth',2,'Color','r');
    set( gca, 'Box','off',...
    'TickDir','out',...
    'TickLength',[.02 .02],...
    'XMinorTick','on',...
    'YMinorTick','on')
    set( gcf, 'PaperPositionMode','auto')
    %legend('abs. Fehler');
    xlabel('\alpha')
    
    errs = zeros(n,2);
    errs(:,1) = alpha;
    errs(:,2) = norms;

end