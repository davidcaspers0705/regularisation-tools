function [res] = adagger(A,y)
    % A2DAGGER calculates norm(A^{\dagger\dagger}y,2)
    % This value relates to the smoothness of the data y.
    n = min(size(A));
    
    [U,S,V] = svd(A);
    adagger = 0;
    for i=1:n
        si = S(i,i);
        if si ~= 0
            adagger = adagger + 1/si*(U(:,i)'*y)*V(:,i);
        end
    end
    res = adagger;
end