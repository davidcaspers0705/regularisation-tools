function [xih] = TSVD(A,yh,alpha)
    % TSVD Returns TSVD-regularised solution of the discrete inverse problem
    % A*xih = yh using the regularisation parameter alpha.
    n = min(size(A));
    [U,S,V] = svd(A);
    sr = 0;  %denotes smallest sv > 0
    
    for i=1:n
        si = S(i,i);
       if si <= alpha
           if si ~=0
               sr = si;
           end
           %deletion of singular values beyond threshold alpha:
           si = 0;
       else
           %other sv's are inverted to obtain pseudoinverse:
           if si ~= 0
               sr = si;
               S(i,i) = 1/si;
           end
       end
    end
    
    %pseudoinverse of regularised matrix:
    A_reg_inv = V*S*U';
    xih = A_reg_inv*yh;
    
    %print error bound for user:
    %{
    [U,S,V] = svd(A);
    a2dagger = 0;
    for i=1:n
        si = S(i,i);
        if si ~= 0
            a2dagger = a2dagger + 1/si^2*(U(:,i)'*yh)*V(:,i);
        end
    end
    max_abs_error = min(1/sr,1/alpha)*norm(dy,2)+alpha*norm(a2dagger,2)
    %}
end

