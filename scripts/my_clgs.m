function [xik] = my_clgs(A,yh,k,eps)
    n=min(size(A));
    d = @(alpha)(norm(A*Landweber(A,y,alpha,y)-y,2)^2);
    tol=1E-16;
    
    b=yh;
    % Prepare for CG iteration.
    x = zeros(n,1);
    d = A'*b;
    r = b;
    normr2 = d'*d;
    for j=1:k
      % Update x and r vectors.
      Ad = A*d; alpha = normr2/(Ad'*Ad);
      x  = x + alpha*d;
      r  = r - alpha*Ad;
      s  = A'*r;
      % Update d vector.
      normr2_new = s'*s;
      beta = normr2_new/normr2;
      normr2 = normr2_new;
      d = s + beta*d;
      if abs(norm(A*x-yh,2)^2-eps^2) <= tol
          disc = abs(norm(A*x-yh,2)^2-eps^2)
          k=j;
          break;
      end
    end
    k
    xik=x;
end