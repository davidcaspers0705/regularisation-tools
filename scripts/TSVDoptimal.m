function [alpha] = TSVDoptimal(eps,A,y,mode)
    % TSVDOPTIMAL returns an optimal reg. par. choice for TSVD method
    % for the returned alpha value, a specific known error bound is
    % minimised depending on model matrix A and (measured) data y,
    % assuming that the error bound eps is known for the perturbation
    % of the data y
    
    % if mode='rel' is passed, returns the parameter which gives the
    % optimal relative error bound.
    % Otherwise: optimal absolute error bound.
    
    if (mode == 'rel')
        sy = norm(adagger(A,y),2)/norm(a2dagger(A,y),2);
        alpha = sqrt(svds(A,1)*(sy*eps));
    else
        alpha = sqrt(eps/norm(a2dagger(A,y),2));
    end
    
end